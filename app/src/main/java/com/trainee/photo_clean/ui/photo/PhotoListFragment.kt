package com.trainee.photo_clean.ui.photo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.trainee.photo_clean.databinding.FragmentPhotoListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoListFragment : Fragment() {
    private val viewModel: PhotoListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentPhotoListBinding.inflate(inflater)
        binding.lifecycleOwner = this

        binding.viewModel = viewModel
// Move to OnViewCreated. block
        binding.photosGrid.adapter = PhotoGridAdapter(PhotoGridAdapter.OnClickListener {
            viewModel.displayPhotoDetails(it)
        })

        viewModel.navigateToSelectedPhoto.observe(viewLifecycleOwner, { photo ->
            photo?.let {
                this.findNavController().navigate(
                    PhotoListFragmentDirections.actionPhotoListFragmentToPhotoDetailFragment(it)
                )
                viewModel.displayPhotoDetailsComplete()
            }
        })
        return binding.root
    }
}