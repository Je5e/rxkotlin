package com.trainee.data.networking.model

import com.trainee.data.database.model.PhotoEntity
import com.trainee.domain.model.photo.Photo

data class PhotoResponse(
    val albumId: Int,
    val id: Int, val title: String, val url: String, val thumbnailUrl: String
)
fun List<PhotoResponse>.asDomainModel(): List<Photo> {
    return map {
        Photo(
            id = it.id,
            albumId = it.albumId,
            title = it.title,
            url = it.url,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}
fun List<PhotoResponse>.asDatabaseModel(): List<PhotoEntity> {
    return map {
        PhotoEntity(
            albumId = it.albumId,
            id = it.id,
            url = it.url,
            title = it.title,
            thumbnailUrl = it.thumbnailUrl)
    }
}