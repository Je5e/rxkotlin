package com.trainee.photo_clean.ui.photo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.skydoves.landscapist.coil.CoilImage
import com.trainee.photo_clean.R
import com.trainee.photo_clean.databinding.FragmentPhotoDetailBinding
import com.trainee.photo_clean.utilities.InjectoUtils
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PhotoDetailFragment : Fragment() {

    private val args: PhotoDetailFragmentArgs by navArgs()
    private val photoDetailVM: PhotoDetailViewModel by viewModels {
        InjectoUtils.providePhotoDetailViewModelFactory(args.selectedPhoto)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<FragmentPhotoDetailBinding>(
            inflater, R.layout.fragment_photo_detail, container, false
        ).apply {

            viewModel = viewModel
            lifecycleOwner = lifecycleOwner

            composeView.setContent {
                MaterialTheme {
                    Surface(color = MaterialTheme.colors.background) {
                        PhotoDetailDescription(photoDetailVM)
                    }
                }
            }
        }
        return binding.root
    }
}

@Composable
private fun PhotoDetailDescription(viewModel: PhotoDetailViewModel) {

    Row(modifier = Modifier.padding(24.dp)) {
        Column(modifier = Modifier.weight(1f)) {
            CoilImage(
                imageModel = viewModel.photo.thumbnailUrl,
                contentScale = ContentScale.Crop,
            )
        }
    }
    Row(modifier = Modifier.padding(24.dp)) {
        Text(text = viewModel.photo.title)

    }
}