package com.trainee.data.repository

import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.asDomainModel
import com.trainee.data.networking.PhotoApi
import com.trainee.data.networking.model.asDatabaseModel
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PhotoRepositoryImp @Inject constructor(
    private val photoApi: PhotoApi,
    private val photoDao: PhotoDao
) : PhotoRepository {

    override fun getAllPhotos(): Flowable<List<Photo>> =
        photoDao.getPhotos().map { it.asDomainModel() }

    override suspend fun refreshPhotos() {
        withContext(Dispatchers.IO) {
            val posts = photoApi.getPhotos()
            photoDao.insertAll(posts.asDatabaseModel())
        }
    }
}