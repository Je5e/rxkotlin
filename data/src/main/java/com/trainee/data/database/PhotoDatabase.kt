package com.trainee.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity

@Database(entities = [PhotoEntity::class], version = 1, exportSchema = false)
abstract class PhotoDatabase : RoomDatabase() {
    abstract val photoDao: PhotoDao


    companion object{
        private lateinit var INSTANCE: PhotoDatabase
        fun getDatabase(context: Context): PhotoDatabase {
            synchronized(PhotoDatabase::class.java) {
                if (!::INSTANCE.isInitialized) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        PhotoDatabase::class.java,
                        "photodb").build()
                }
            }
            return INSTANCE
        }

    }


}