package com.trainee.data.di

import android.content.Context
import com.trainee.data.database.PhotoDatabase
import com.trainee.data.database.dao.PhotoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun providePhotoDatabase(@ApplicationContext context: Context): PhotoDatabase {
        return PhotoDatabase.getDatabase(context)
    }

    @Provides
    fun providePhotoDao(appDatabase: PhotoDatabase): PhotoDao {
        return appDatabase.photoDao
    }


}