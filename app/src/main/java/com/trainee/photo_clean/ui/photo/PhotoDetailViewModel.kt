package com.trainee.photo_clean.ui.photo

import androidx.lifecycle.ViewModel
import com.trainee.domain.model.photo.Photo

class PhotoDetailViewModel(
    val photo: Photo
) : ViewModel()