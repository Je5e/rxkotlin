package com.trainee.photo_clean
import android.app.Application
import androidx.databinding.library.BuildConfig
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
    class PhotoApplication : Application(), Configuration.Provider {

        override fun getWorkManagerConfiguration(): Configuration =
            Configuration.Builder()
                .setMinimumLoggingLevel(if (BuildConfig.DEBUG) android.util.Log.DEBUG else android.util.Log.ERROR)
                .build()
    }
