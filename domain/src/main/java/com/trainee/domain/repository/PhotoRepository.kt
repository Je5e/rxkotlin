package com.trainee.domain.repository

import androidx.lifecycle.LiveData
import com.trainee.domain.model.photo.Photo
import io.reactivex.rxjava3.core.Flowable

interface PhotoRepository {
     fun getAllPhotos(): Flowable<List<Photo>> // MutableLiveEvent

    suspend fun refreshPhotos()
}