package com.trainee.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.trainee.domain.model.photo.Photo

@Entity
class PhotoEntity(
    @PrimaryKey
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)

fun List<PhotoEntity>.asDomainModel(): List<Photo> {
    return map {
        Photo(
            id = it.id,
            albumId = it.albumId,
            title = it.title,
            url = it.url,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}