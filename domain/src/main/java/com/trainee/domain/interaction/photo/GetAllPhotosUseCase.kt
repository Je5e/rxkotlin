package com.trainee.domain.interaction.photo

import androidx.lifecycle.LiveData
import com.trainee.domain.model.photo.Photo
import io.reactivex.rxjava3.core.Flowable

interface GetAllPhotosUseCase {
     operator fun invoke(): Flowable<List<Photo>>
    // fun invoke(): Flowable<List<Photo>>
}