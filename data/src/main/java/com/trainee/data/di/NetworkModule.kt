package com.trainee.data.di

import com.trainee.data.networking.PhotoApi
import com.trainee.data.networking.PhotoService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providePhotoService(): PhotoApi {
        return PhotoService.retrofitService
    }
}