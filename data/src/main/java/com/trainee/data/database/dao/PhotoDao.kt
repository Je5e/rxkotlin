package com.trainee.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trainee.data.database.model.PhotoEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface PhotoDao {
    @Query("select * from photoentity")
    //fun getPhotos(): LiveData<List<PhotoEntity>>
    fun getPhotos(): Flowable<List<PhotoEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll( photos: List<PhotoEntity>)
}