package com.trainee.photo_clean.ui.photo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.trainee.domain.model.photo.Photo

class PhotoDetailViewModelFactory(var photo: Photo) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = PhotoDetailViewModel(photo) as T
}