package com.trainee.photo_clean.utilities

import android.content.Context
import com.trainee.domain.model.photo.Photo
import com.trainee.photo_clean.ui.photo.PhotoDetailViewModelFactory

object InjectoUtils {

    fun providePhotoDetailViewModelFactory(
        photo: Photo
    ): PhotoDetailViewModelFactory {
        return PhotoDetailViewModelFactory(
            photo = photo
        )
    }
}