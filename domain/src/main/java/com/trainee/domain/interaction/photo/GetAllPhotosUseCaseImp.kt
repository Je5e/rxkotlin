package com.trainee.domain.interaction.photo

import com.trainee.domain.repository.PhotoRepository

class GetAllPhotosUseCaseImp(private val repository: PhotoRepository) : GetAllPhotosUseCase {
    override  fun invoke() = repository.getAllPhotos()
    //override fun invoke() = repository.getAllPhotos()
}