package com.trainee.domain.interaction.photo

interface RefreshPhotosUseCase {
    suspend operator fun invoke()
}