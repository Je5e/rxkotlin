package com.trainee.data.di

import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.networking.PhotoApi
import com.trainee.data.repository.PhotoRepositoryImp
import com.trainee.domain.repository.PhotoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {


    @Provides
    fun providePhotoRepository(
        photoAPi: PhotoApi,
        photoDao: PhotoDao
    ): PhotoRepository {
        return PhotoRepositoryImp(photoAPi, photoDao)
    }


}