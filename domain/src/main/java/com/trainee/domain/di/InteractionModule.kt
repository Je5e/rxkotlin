package com.trainee.domain.di

import com.trainee.domain.interaction.photo.GetAllPhotosUseCase
import com.trainee.domain.interaction.photo.GetAllPhotosUseCaseImp
import com.trainee.domain.interaction.photo.RefreshPhotosUseCase
import com.trainee.domain.interaction.photo.RefreshPhotosUseCaseImp
import com.trainee.domain.repository.PhotoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class InteractionModule {

    @Provides
    fun provideGetAllPhotosUseCase(repository: PhotoRepository): GetAllPhotosUseCase {
        return GetAllPhotosUseCaseImp(repository)
    }

    @Provides
    fun provideRefreshPhotosUseCase(repository: PhotoRepository): RefreshPhotosUseCase {
        return RefreshPhotosUseCaseImp(repository)
    }
}