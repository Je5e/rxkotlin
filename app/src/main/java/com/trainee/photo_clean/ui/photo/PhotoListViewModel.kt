package com.trainee.photo_clean.ui.photo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.domain.interaction.photo.GetAllPhotosUseCase
import com.trainee.domain.interaction.photo.RefreshPhotosUseCase
import com.trainee.domain.model.photo.Photo
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotoListViewModel @Inject constructor(
    private val getAllPhotosUseCase: GetAllPhotosUseCase,
    private val refreshPhotosUseCase: RefreshPhotosUseCase
) :
    ViewModel() {
    val subscriptions = CompositeDisposable()

    private val _navigateToSelectedPhoto = MutableLiveData<Photo>()
    val navigateToSelectedPhoto: LiveData<Photo>
        get() = _navigateToSelectedPhoto

    /**
     * Call getPhotos() on init so we can display status immediately.
     */
    init {
        getPhotosFromRepository()
    }

    fun getPhotos(): Flowable<List<Photo>> = getAllPhotosUseCase().map { list -> list }

    private fun getPhotosFromRepository() {
        viewModelScope.launch {
            try {
                refreshPhotosUseCase()
            } catch (e: Exception) { // hacer catch excepción específica.
                Log.d("ERROR","vmp")
            }
        }
    }

    fun displayPhotoDetails(photo: Photo) {
        _navigateToSelectedPhoto.value = photo
    }

    fun displayPhotoDetailsComplete() {
        _navigateToSelectedPhoto.value = null
    }

    override fun onCleared() {
        subscriptions.dispose()
        super.onCleared()
    }
}